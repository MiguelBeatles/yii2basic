<?php
namespace app\models;

use yii;

class Calculadora extends \yii\widgets\ActiveForm
{
    public $operando1;
    public $operando2;
    public $operador;

    public function attributeLabels()
    {
      return [
        'operando1'=>'X',
        'operando2'=>'Y',
      ];

    }

    public function rules()
    {
      return [
          [['operando1', 'operando2'], 'required'],
          [['operando1','operando2'], 'float'],
          ['operador','in','range'=>['+','-','*','/']],
          ['operando2', 'validarDivCero'],
      ];
    }

    public function validarDivCero($attribute, $params)
    {
        if($this -> operador == '/' && $this -> operando2 == 0)
            $this -> addError('operando2', 'El denominador de una división no puede ser cero.');
    }

    public function safeAttributes()
    {
        return array[
            'operando1, operando2, operador'
        ];
    }

    public function operar()
    {
      $r=0;
        switch ($this->operador)
        {
          case '+': $r=$this->operando1+$this->operando2;
          break;
          case '-': $r=$this->operando1-$this->operando2;
          break;
          case '*': $r=$this->operando1*$this->operando2;
          break;
          case '/': $r=$this->operando1/$this->operando2;
          break;
      }
    }

}


 ?>
