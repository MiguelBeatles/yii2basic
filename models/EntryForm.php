<?php

namespace app\models;

use Yii;
use yii\base\Model;

class EntryForm extends Model
{
    public $name;
    public $email;
    public $title;
    public $mensaje;

    public function rules()
    {
        return [
            [['name', 'email','title','mensaje'], 'required'],
            ['email', 'email'],
        ];
    }
}
