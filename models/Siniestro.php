<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "siniestro".
 *
 * @property integer $id
 * @property integer $id_creador
 * @property string $id_resguardo
 * @property string $inventario
 * @property string $territorial
 * @property string $status
 * @property integer $id_localizador
 * @property string $localizador
 * @property string $siniestro
 * @property string $serie
 * @property string $marca
 * @property string $motor
 * @property string $tipo
 * @property string $modelo
 * @property string $placa
 * @property string $na_t
 * @property string $causa
 * @property string $asegurado
 * @property string $poliza
 * @property string $inciso
 * @property string $broker_agente
 * @property string $tipo_cuenta
 * @property string $fecha_siniestro
 * @property string $fecha_ingreso
 * @property string $fecha_asignacion_pt
 * @property string $primer_contacto
 * @property string $fecha_carta_pt
 * @property string $ultimo_contacto
 * @property string $fecha_documentacion
 * @property string $fecha_pago
 * @property string $pagador
 * @property string $telefono_o_celular
 * @property string $validacion_telefono
 * @property string $correo
 * @property string $validacion_correo
 * @property string $direccion_asegurado
 * @property string $validacion_direccion
 * @property string $financiera
 * @property string $financiamiento
 * @property string $dua
 * @property string $carta_pt
 * @property integer $dias_resguardo
 * @property string $antiguedad_resguardo
 * @property integer $dias_siniestro
 * @property string $antiguedad_siniestro
 * @property string $antiguedad_modelo
 * @property string $categoria_unidad
 * @property string $tramite_gestoria
 * @property string $f_rechazo
 * @property string $f_a_gestoria
 * @property string $fini_asegurado
 * @property string $f_ini_gestoria
 * @property string $f_fin_gestoria
 * @property string $fecha_salvamentos
 * @property string $fecha_promesa_pago
 * @property integer $orden_pago
 * @property string $fecha_finiquito_gestoria
 * @property string $fecha_comercializacion
 * @property string $status_egresos
 * @property string $tramite
 * @property string $tipo_gestoria
 * @property string $tenencias
 * @property string $ordenes_pago
 * @property string $fecha_alta
 * @property string $fecha_actualizacion
 *
 * @property Agenda[] $agendas
 * @property Seguimiento[] $seguimientos
 * @property Tramite[] $tramites
 */
class Siniestro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'siniestro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_creador', 'id_localizador', 'dias_resguardo', 'dias_siniestro', 'orden_pago'], 'integer'],
            [['id_localizador'], 'required'],
            [['fecha_siniestro', 'fecha_ingreso', 'fecha_asignacion_pt', 'primer_contacto', 'fecha_carta_pt', 'ultimo_contacto', 'fecha_documentacion', 'fecha_pago', 'f_rechazo', 'f_a_gestoria', 'fini_asegurado', 'f_ini_gestoria', 'f_fin_gestoria', 'fecha_salvamentos', 'fecha_promesa_pago', 'fecha_finiquito_gestoria', 'fecha_comercializacion', 'fecha_alta', 'fecha_actualizacion'], 'safe'],
            [['id_resguardo', 'inventario', 'territorial', 'status', 'localizador', 'siniestro', 'serie', 'marca', 'motor', 'tipo', 'modelo', 'placa', 'na_t', 'causa', 'poliza', 'inciso', 'tipo_cuenta', 'pagador', 'validacion_direccion', 'antiguedad_resguardo', 'antiguedad_siniestro', 'antiguedad_modelo', 'categoria_unidad', 'tramite_gestoria', 'status_egresos', 'tipo_gestoria', 'tenencias'], 'string', 'max' => 45],
            [['asegurado', 'broker_agente', 'telefono_o_celular', 'correo', 'direccion_asegurado', 'financiera'], 'string', 'max' => 255],
            [['validacion_telefono', 'validacion_correo', 'financiamiento', 'dua', 'carta_pt'], 'string', 'max' => 2],
            [['tramite', 'ordenes_pago'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_creador' => 'Id Creador',
            'id_resguardo' => 'Id Resguardo',
            'inventario' => 'Inventario',
            'territorial' => 'Territorial',
            'status' => 'Status',
            'id_localizador' => 'Id Localizador',
            'localizador' => 'Localizador',
            'siniestro' => 'Siniestro',
            'serie' => 'Serie',
            'marca' => 'Marca',
            'motor' => 'Motor',
            'tipo' => 'Tipo',
            'modelo' => 'Modelo',
            'placa' => 'Placa',
            'na_t' => 'Na T',
            'causa' => 'Causa',
            'asegurado' => 'Asegurado',
            'poliza' => 'Poliza',
            'inciso' => 'Inciso',
            'broker_agente' => 'Broker Agente',
            'tipo_cuenta' => 'Tipo Cuenta',
            'fecha_siniestro' => 'Fecha Siniestro',
            'fecha_ingreso' => 'Fecha Ingreso',
            'fecha_asignacion_pt' => 'Fecha Asignacion Pt',
            'primer_contacto' => 'Primer Contacto',
            'fecha_carta_pt' => 'Fecha Carta Pt',
            'ultimo_contacto' => 'Ultimo Contacto',
            'fecha_documentacion' => 'Fecha Documentacion',
            'fecha_pago' => 'Fecha Pago',
            'pagador' => 'Pagador',
            'telefono_o_celular' => 'Telefono O Celular',
            'validacion_telefono' => 'Validacion Telefono',
            'correo' => 'Correo',
            'validacion_correo' => 'Validacion Correo',
            'direccion_asegurado' => 'Direccion Asegurado',
            'validacion_direccion' => 'Validacion Direccion',
            'financiera' => 'Financiera',
            'financiamiento' => 'Financiamiento',
            'dua' => 'Dua',
            'carta_pt' => 'Carta Pt',
            'dias_resguardo' => 'Dias Resguardo',
            'antiguedad_resguardo' => 'Antiguedad Resguardo',
            'dias_siniestro' => 'Dias Siniestro',
            'antiguedad_siniestro' => 'Antiguedad Siniestro',
            'antiguedad_modelo' => 'Antiguedad Modelo',
            'categoria_unidad' => 'Categoria Unidad',
            'tramite_gestoria' => 'Tramite Gestoria',
            'f_rechazo' => 'F Rechazo',
            'f_a_gestoria' => 'F A Gestoria',
            'fini_asegurado' => 'Fini Asegurado',
            'f_ini_gestoria' => 'F Ini Gestoria',
            'f_fin_gestoria' => 'F Fin Gestoria',
            'fecha_salvamentos' => 'Fecha Salvamentos',
            'fecha_promesa_pago' => 'Fecha Promesa Pago',
            'orden_pago' => 'Orden Pago',
            'fecha_finiquito_gestoria' => 'Fecha Finiquito Gestoria',
            'fecha_comercializacion' => 'Fecha Comercializacion',
            'status_egresos' => 'Status Egresos',
            'tramite' => 'Tramite',
            'tipo_gestoria' => 'Tipo Gestoria',
            'tenencias' => 'Tenencias',
            'ordenes_pago' => 'Ordenes Pago',
            'fecha_alta' => 'Fecha Alta',
            'fecha_actualizacion' => 'Fecha Actualizacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendas()
    {
        return $this->hasMany(Agenda::className(), ['id_siniestro' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeguimientos()
    {
        return $this->hasMany(Seguimiento::className(), ['id_siniestro' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTramites()
    {
        return $this->hasMany(Tramite::className(), ['id_siniestro' => 'id']);
    }
}
