<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tramite".
 *
 * @property integer $id
 * @property integer $id_siniestro
 * @property integer $id_creador
 * @property string $concepto
 * @property string $monto
 * @property string $fecha_alta
 * @property string $fecha_actualizacion
 *
 * @property User $idCreador
 * @property Siniestro $idSiniestro
 */
class Tramite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tramite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_siniestro'], 'required'],
            [['id_siniestro', 'id_creador'], 'integer'],
            [['monto'], 'number'],
            [['fecha_alta', 'fecha_actualizacion'], 'safe'],
            [['concepto'], 'string', 'max' => 255],
            [['id_creador'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_creador' => 'id']],
            [['id_siniestro'], 'exist', 'skipOnError' => true, 'targetClass' => Siniestro::className(), 'targetAttribute' => ['id_siniestro' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_siniestro' => 'Id Siniestro',
            'id_creador' => 'Id Creador',
            'concepto' => 'Concepto',
            'monto' => 'Monto',
            'fecha_alta' => 'Fecha Alta',
            'fecha_actualizacion' => 'Fecha Actualizacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCreador()
    {
        return $this->hasOne(User::className(), ['id' => 'id_creador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSiniestro()
    {
        return $this->hasOne(Siniestro::className(), ['id' => 'id_siniestro']);
    }
}
