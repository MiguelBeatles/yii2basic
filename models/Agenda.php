<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agenda".
 *
 * @property integer $id
 * @property integer $id_siniestro
 * @property integer $id_creador
 * @property integer $id_actualizador
 * @property integer $id_analista
 * @property string $titulo
 * @property string $fecha_inicio_cita
 * @property string $fecha_fin_cita
 * @property integer $eliminado
 * @property string $fecha_borrado
 * @property string $fecha_alta
 * @property string $fecha_actualizacion
 *
 * @property User $idActualizador
 * @property User $idAnalista
 * @property User $idCreador
 * @property Siniestro $idSiniestro
 * @property AgendaFecha[] $agendaFechas
 */
class Agenda extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agenda';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_siniestro', 'id_creador', 'id_actualizador', 'id_analista', 'titulo'], 'required'],
            [['id_siniestro', 'id_creador', 'id_actualizador', 'id_analista', 'eliminado'], 'integer'],
            [['fecha_inicio_cita', 'fecha_fin_cita', 'fecha_borrado', 'fecha_alta', 'fecha_actualizacion'], 'safe'],
            [['titulo'], 'string', 'max' => 100],
            [['id_actualizador'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_actualizador' => 'id']],
            [['id_analista'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_analista' => 'id']],
            [['id_creador'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_creador' => 'id']],
            [['id_siniestro'], 'exist', 'skipOnError' => true, 'targetClass' => Siniestro::className(), 'targetAttribute' => ['id_siniestro' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_siniestro' => 'Id Siniestro',
            'id_creador' => 'Id Creador',
            'id_actualizador' => 'Id Actualizador',
            'id_analista' => 'Id Analista',
            'titulo' => 'Titulo',
            'fecha_inicio_cita' => 'Fecha Inicio Cita',
            'fecha_fin_cita' => 'Fecha Fin Cita',
            'eliminado' => 'Eliminado',
            'fecha_borrado' => 'Fecha Borrado',
            'fecha_alta' => 'Fecha Alta',
            'fecha_actualizacion' => 'Fecha Actualizacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdActualizador()
    {
        return $this->hasOne(User::className(), ['id' => 'id_actualizador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAnalista()
    {
        return $this->hasOne(User::className(), ['id' => 'id_analista']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCreador()
    {
        return $this->hasOne(User::className(), ['id' => 'id_creador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSiniestro()
    {
        return $this->hasOne(Siniestro::className(), ['id' => 'id_siniestro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendaFechas()
    {
        return $this->hasMany(AgendaFecha::className(), ['id_agenda' => 'id']);
    }
}
