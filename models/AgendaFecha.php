<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agenda_fecha".
 *
 * @property integer $id
 * @property integer $id_agenda
 * @property integer $id_creador
 * @property string $motivo
 * @property string $inicio
 * @property string $fin
 * @property integer $eliminado
 * @property string $fecha_borrado
 * @property string $fecha_alta
 * @property string $fecha_actualizacion
 *
 * @property Agenda $idAgenda
 * @property User $idCreador
 */
class AgendaFecha extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agenda_fecha';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_agenda', 'id_creador', 'inicio', 'fin', 'fecha_alta', 'fecha_actualizacion'], 'required'],
            [['id_agenda', 'id_creador', 'eliminado'], 'integer'],
            [['inicio', 'fin', 'fecha_borrado', 'fecha_alta', 'fecha_actualizacion'], 'safe'],
            [['motivo'], 'string', 'max' => 100],
            [['id_agenda'], 'exist', 'skipOnError' => true, 'targetClass' => Agenda::className(), 'targetAttribute' => ['id_agenda' => 'id']],
            [['id_creador'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_creador' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_agenda' => 'Id Agenda',
            'id_creador' => 'Id Creador',
            'motivo' => 'Motivo',
            'inicio' => 'Inicio',
            'fin' => 'Fin',
            'eliminado' => 'Eliminado',
            'fecha_borrado' => 'Fecha Borrado',
            'fecha_alta' => 'Fecha Alta',
            'fecha_actualizacion' => 'Fecha Actualizacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAgenda()
    {
        return $this->hasOne(Agenda::className(), ['id' => 'id_agenda']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCreador()
    {
        return $this->hasOne(User::className(), ['id' => 'id_creador']);
    }
}
