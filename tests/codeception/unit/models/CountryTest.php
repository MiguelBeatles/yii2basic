<?php
namespace app\tests\models;

use yii\codeception\DbTestCase;
use app\tests\fixtures\CountryFixture;


class CountryTest extends DbTestCase
{
    public function fixtures()
    {
        return [
            'countries' => CountryFixture::className(),
        ];
    }
    // ...test methods...
    // returns the data row aliased as 'user1'
$row = $this->countries['country1'];
// returns the UserProfile model corresponding to the data row aliased as 'user1'
//$profile = $this->countries('country2');
// traverse every data row in the fixture
//foreach ($this->countries as $row);
}
