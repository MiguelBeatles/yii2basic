<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\swiftmailer\Mailer;
?>
<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'mensaje') ?>

    <div class="form-group">


        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>
