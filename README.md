Yii 2 Basic Project Template
============================

Yii 2 Basic Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
rapidly creating small projects.

The template contains the basic features including user login/logout and a contact page.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application.

[![Latest Stable Version](https://poser.pugx.org/yiisoft/yii2-app-basic/v/stable.png)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Total Downloads](https://poser.pugx.org/yiisoft/yii2-app-basic/downloads.png)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-basic.svg?branch=master)](https://travis-ci.org/yiisoft/yii2-app-basic)

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

### Install from an Archive File

Extract the archive file downloaded from [yiiframework.com](http://www.yiiframework.com/download/) to
a directory named `basic` that is directly under the Web root.

Set cookie validation key in `config/web.php` file to some random secret string:

```php
'request' => [
    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
    'cookieValidationKey' => '<secret random string goes here>',
],
```

You can then access the application through the following URL:

~~~
http://localhost/basic/web/
~~~


### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar create-project --prefer-dist --stability=dev yiisoft/yii2-app-basic basic
~~~

Now you should be able to access the application through the following URL, assuming `basic` is the directory
directly under the Web root.

~~~
http://localhost/basic/web/
~~~


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `config/` directory to customize your application as required.
- Refer to the README in the `tests` directory for information specific to basic application tests.


COMO USARLO
------------

Para empezar debemos de tener presente como funcionan los fixtures para poder saber como utilizarlos.


FIXTURES
------------

Los FIXTURES son una parte importante de las pruebas. Su propósito principal es el de configurar el entorno en un estado fijo / conocido para que sus pruebas que son repetibles se ejecutan de una manera rapida.

DECLARAR UN FIXTURES
------------
**NOTES:**

- Como primer dato para generar un fixture  necesitamos el namespace que es la carpeta en donde se localiza el archivo en nuestro caso  cada tabla tendra unmodelo y un fixture para ese modelo con el mismo nombre pero con el sufijo fixture, estos se crean en la carpeta fixtures, el segundo dato es la clase de donde va a extender que es ActiveFixture, por ultimo se crea la clse que debe de llamarse igual que el archivo y dentro de este se especifica la direccion del modelo y si tiene dependencias.

      <?php
        namespace app\tests\fixtures;

        use yii\test\ActiveFixture;

        class UserFixture extends ActiveFixture
        {
          public $modelClass = 'app\models\User';
          public $depends = ['app\tests\fixtures\TramiteFixture'];
        }





- Como segundo paso temenos que realizar la carpeta de data que sirve para que se genere la informacion, esta se crea dentro de la carpeta de fixtures ahi los templates depositan la informacion


       ```php

          return [
          'user1' => [
                  'username' => 'lmayert',
                  'email' => 'strosin.vernice@jerde.com',
                  'auth_key' => 'K3nF70it7tzNsHddEiq0BZ0i-OU8S3xV',
                  'password' => '$2y$13$WSyE5hHsG1rWN2jV8LRHzubilrCLI5Ev/iK0r3jRuwQEs2ldRu.a2',
                  ],
             'user2' => [
                 'username' => 'napoleon69',
                 'email' => 'aileen.barton@heaneyschumm.com',
                 'auth_key' => 'dZlXsVnIDgIzFgX4EduAqkEPuphhOh9q',
                 'password' => '$2y$13$kkgpvJ8lnjKo8RuoR30ay.RjDf15bMcHIF7Vz1zz/6viYG5xJExU6',
                 ],
                    ];
        ```


- el tercer paso es generar los templates en donde vamos a generar automaticamente los datos para poblar la base de datos con una herramienta que se llama faker, como podemos ver mencionamos la tabla llamamos la clase faker y pedimos el valor ramdom esto generaran los data que mencionamos anteriormente.


           $connection->createCommand()->batchInsert('agenda', ['id_siniestro', 
	       'id_creador','id_actualizador','id_analista','titulo','fecha_inicio_cita','fecha_fin_cita',
            'eliminado','fecha_borrado','fecha_alta','fecha_actualizacion'], [
            ['id_siniestro' => $faker->randomNumber($nbDigits = NULL),
           'id_creador' => $faker->randomNumber($nbDigits = NULL),
           'id_actualizador' => $faker->randomNumber($nbDigits = NULL),
           'id_analista' => $faker->randomNumber($nbDigits = NULL),
           'titulo' => $faker->sentence($nbWords = 3, $variableNbWords =NULL) ,
            'fecha_inicio_cita'=>$faker->dateTimeThisYear($max =   'now')->format('Y-m-d H:i:s') ,
            'fecha_fin_cita'=>$faker->dateTime()->format('Y-m-d H:i:s'),
            'eliminado'=>$faker->randomDigit,
            'fecha_borrado'=>$faker->dateTime()->format('Y-m-d H:i:s'),
            'fecha_alta'=>$faker->dateTime()->format('Y-m-d H:i:s'),
            'fecha_actualizacion'=>$faker->dateTime()->format('Y-m-d H:i:s')]
            ])->execute();

EJECUTAR FIXTURES
------------


Para ejecutar los fixtures entramos a la consola y escribimos los siguientes comandos.


  